package com.pawelbanasik;

public interface SeriesGenerator {
	int generate(int i);
}
